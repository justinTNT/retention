import R from 'ramda'

import environments from '../data/Environments.json'
import projects from '../data/Projects.json'
import releases from '../data/Releases.json'

const findById = data => id =>
    R.find( R.propEq('Id', id), data )

const nameFromId = data =>
    R.compose(
        R.prop('Name'),
        findById(data)
    )

const getEnvName = nameFromId(environments);
const getProjectName = nameFromId(projects);

const isValidId = data =>
    R.compose(
        R.not,
        R.isNil,
        findById(data)
    )

const isValidEnv = isValidId(environments);
const isValidProject = isValidId(projects);

const findRelease = findById(releases);

export default {
    findRelease,
    getEnvName,
    getProjectName,
    isValidEnv,
    isValidProject
}
