import R from 'ramda'

import { humanizeIndex, log } from './utils/common-utils.js'
import Data from './utils/data-utils.js'

const logReasonForRetention = (value, key) => {
    if (value) {
        const len = value.length
        for (let i=0; i < value.length; i++) {
            const release = value[i];
            const proj = Data.getProjectName( release.ProjectId );
            const env = Data.getEnvName( key.substring(release.ProjectId.length) );

            log(`Retaining ${release.Id} because it is the ${humanizeIndex(i)}most recent ${env} deployment for ${proj}`)
        }
    }
}

const retention = scope => deployments => {
    const releaseDictionary = new Map();
    const retainedIds = [];

    const considerInserting = deployment => {
        if (deployment && Data.isValidEnv(deployment.EnvironmentId)) {
            const release = Data.findRelease(deployment.ReleaseId)

            if (release
                && Data.isValidProject(release.ProjectId)
                && !retainedIds.includes(release.Id)) {

                const key = release.ProjectId + deployment.EnvironmentId;
                const stored = releaseDictionary.get(key) || [];

                if ( stored.length < scope ) {
                    releaseDictionary.set(key, stored.concat([release]));
                    retainedIds.push(release.Id)
                }
            }
        }
    }

    R.compose(
        R.map(considerInserting),
        R.sortWith([R.descend(
            R.compose(
                Date.parse,
                R.prop('DeployedAt')
            )
        )])
    )(deployments);

    releaseDictionary.forEach(logReasonForRetention);

    return R.flatten([ ...releaseDictionary.values() ])
}

export { retention }
