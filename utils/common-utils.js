// could probably use something more robust
const log = console.log;

// converts zero-based array index to humanized string, with blank for first
const humanizeIndex = number => {
    const suffix = ["th", "st", "nd", "rd"];
    const value = (number + 1) % 100;

    return number
        ?  `${number + 1}${suffix[(value-20)%10] || suffix[value] || suffix[0]} `
        : ""
}

export { humanizeIndex, log }
