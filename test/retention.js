import assert from 'assert'
import sinon from 'sinon'
import R from 'ramda'

import { retention } from '../retention.js'
import deployments from '../data/Deployments.json'
import Data from '../utils/data-utils.js'

describe('Release retention', () => {
    let length = 0;

    describe('empty data', () => {
        it('should return nothing when the list of deployments is empty ', () =>
            assert.equal( 0, retention(2)([]).length )
        )

        it('should return nothing when the depth of retention is zero ', () =>
            assert.equal( 0, retention(0)(deployments).length )
        )
    })

    describe('non-empty data', () => {
        it('should return something when the depth of retention is 1', () => {
            assert( 0 < retention(1)(deployments).length );
        })

        it('should return more when the depth of retention is 2 ', () =>
            assert( length < retention(2)(deployments).length )
        )

        it('should return more again when the depth of retention is 3 ', () =>
            assert( retention(3)(deployments).length > retention(2)(deployments).length )
        )
    })

    describe('validity', () => {
        before( () => {
            const oldFinder = Data.findRelease;
            const savedFinder = sinon.stub(Data, 'findRelease').callsFake(id => {
                const ret = oldFinder(id);
                if (ret && id == 'Release-2') {
                    return R.assoc('ProjectId', 'INVALID')
                } else {
                    return ret
                }
            });
        })

        after( () => {
             sinon.restore();
        })

        it('should be resilient to invalid projects', () => {
            assert.equal( retention(1)(deployments).length , 2)
        })

        it('should ignore deployments for non-existent environments', () => {
            const withInvalidEnv = R.map(
                R.when(R.propEq('ReleaseId', 'Release-1'), R.assoc('EnvironmentId', 'INVALID')),
            )(deployments)

            assert.equal( retention(1)(withInvalidEnv).length , 1)
        })

        it('should be resilient to invalid releases', () => {
            const withInvalidRelease = R.map(
                R.when(R.propEq('ReleaseId', 'Release-1'), R.assoc('ReleaseId', 'INVALID'))
            )(deployments)

            assert.equal( retention(1)(withInvalidRelease).length , 1)
        })
    })
})
